/* File: PingDataUplifter.i */
%module PingDataUplifterModule
%{
#include "PingDataUplifter.h"
%}

%javaexception("java.lang.Exception") addBitmapToStitch {
  try {
     $action
  } catch(const UplifterException& e) {
       AndroidBitmap_unlockPixels(env, bitmap);
       jclass je = env->FindClass("java/lang/Exception");
       env->ThrowNew(je, e.what());
       return;
  } catch(const cv::Exception& e) {
    AndroidBitmap_unlockPixels(env, bitmap);
    jclass je = env->FindClass("java/lang/Exception");
    env->ThrowNew(je, e.what());
    return;
  } catch (...) {
    AndroidBitmap_unlockPixels(env, bitmap);
    jclass je = env->FindClass("java/lang/Exception");
    env->ThrowNew(je, "Unknown exception in JNI code {nBitmapToMat}");
    return;
  }
}


class PingDataUplifter {
public:
    std::string stringFromJNI();
    void addBitmapToStitch(JNIEnv * env, jobject bitmap);


private:
    std::vector<cv::Mat> m_images;
};
