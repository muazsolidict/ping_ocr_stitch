
#include "PingDataUplifter.h"


#ifdef __cplusplus
extern "C" {
#endif

jstring JNICALL Java_pingdata_io_pingdataandroidlibrary_PingDataUplifterJNI_stringFromJNI(JNIEnv *jenv, jlong jarg1) {
  PingDataUplifter *uplifter = *(PingDataUplifter **)&jarg1;
  std::string result = (uplifter)->stringFromJNI();
  return jenv->NewStringUTF(result.c_str());
}


void JNICALL Java_pingdata_io_pingdataandroidlibrary_PingDataUplifterJNI_addBitmapToStitch(JNIEnv *jenv, jlong jarg1, jobject bitmap) {
  PingDataUplifter *uplifter = *(PingDataUplifter **)&jarg1;
  {
    try {
      uplifter->addBitmapToStitch(jenv, bitmap);
    } catch(const UplifterException& e) {
      AndroidBitmap_unlockPixels(jenv, bitmap);
      jclass je = jenv->FindClass("java/lang/Exception");
      jenv->ThrowNew(je, e.what());
      return;
    } catch(const cv::Exception& e) {
      AndroidBitmap_unlockPixels(jenv, bitmap);
      jclass je = jenv->FindClass("java/lang/Exception");
      jenv->ThrowNew(je, e.what());
      return;
    } catch (...) {
      AndroidBitmap_unlockPixels(jenv, bitmap);
      jclass je = jenv->FindClass("java/lang/Exception");
      jenv->ThrowNew(je, "Unknown exception in JNI code {nBitmapToMat}");
      return;
    }
  }
}

jint JNICALL Java_pingdata_io_pingdataandroidlibrary_PingDataUplifterJNI_stitch(JNIEnv *jenv, jlong jarg1) {
}

jobject JNICALL Java_pingdata_io_pingdataandroidlibrary_PingDataUplifterJNI_stitchedImage(JNIEnv *jenv, jlong jarg1) {
}


jlong JNICALL Java_pingdata_io_pingdataandroidlibrary_PingDataUplifterJNI_newPingDataUplifter(JNIEnv *jenv, jclass jcls) {
  jlong jresult = 0 ;
  PingDataUplifter *result = 0 ;

  (void)jenv;
  (void)jcls;
  result = (PingDataUplifter *)new PingDataUplifter();
  *(PingDataUplifter **)&jresult = result; 
  return jresult;
}


void JNICALL Java_pingdata_io_pingdataandroidlibrary_PingDataUplifterJNI_deletePingDataUplifter(JNIEnv *jenv, jclass jcls, jlong jarg1) {
  PingDataUplifter *arg1 = (PingDataUplifter *) 0 ;
  
  (void)jenv;
  (void)jcls;
  arg1 = *(PingDataUplifter **)&jarg1; 
  delete arg1;
}


#ifdef __cplusplus
}
#endif

