
#include "PingDataUplifter.h"

std::string PingDataUplifter::stringFromJNI() {
    std::string hello = "Hello from C++";
    return hello;
}

void PingDataUplifter::addBitmapToStitch(JNIEnv * env, jobject bitmap) {
    AndroidBitmapInfo  info;
    void*              pixels = 0;
    cv::Mat dst;
    if (AndroidBitmap_getInfo(env, bitmap, &info) < 0) {
        throw new UplifterException("error in getting bitmap info");
    }
    if( info.format != ANDROID_BITMAP_FORMAT_RGBA_8888 && info.format != ANDROID_BITMAP_FORMAT_RGB_565 ) {
        throw new UplifterException("invalid bitmap format");
    }
    if( AndroidBitmap_lockPixels(env, bitmap, &pixels) < 0 ) {
        throw new UplifterException("error in locking bitmap pixels");
    }
    if ( !pixels ) {
        throw new UplifterException("bitmap has null pixels");
    }
    dst.create(info.height, info.width, CV_8UC4);
    if( info.format == ANDROID_BITMAP_FORMAT_RGBA_8888 ) {
        cv::Mat tmp(info.height, info.width, CV_8UC4, pixels);
        //if(needUnPremultiplyAlpha) cvtColor(tmp, dst, COLOR_mRGBA2RGBA);
        tmp.copyTo(dst);
    } else {
        // info.format == ANDROID_BITMAP_FORMAT_RGB_565
        cv::Mat tmp(info.height, info.width, CV_8UC2, pixels);
        cv::cvtColor(tmp, dst, cv::COLOR_BGR5652RGBA);
    }
    AndroidBitmap_unlockPixels(env, bitmap);
    this->m_images.push_back(dst);
}