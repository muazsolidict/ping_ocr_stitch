
#ifndef PING_DATA_UPLIFTER_H
#define PING_DATA_UPLIFTER_H

#include <string>
#include <vector>
#include <exception>
#include <opencv2/opencv.hpp>

#include <android/bitmap.h>

class UplifterException : public std::exception {
public:
    UplifterException(const char* what) noexcept : m_what(what) {};

    const char* what() const noexcept { return m_what.c_str(); }

private:
    std::string m_what;
};

class PingDataUplifter {
public:
    std::string stringFromJNI();
    void addBitmapToStitch(JNIEnv * env, jobject bitmap);


private:
    std::vector<cv::Mat> m_images;
};

#endif
