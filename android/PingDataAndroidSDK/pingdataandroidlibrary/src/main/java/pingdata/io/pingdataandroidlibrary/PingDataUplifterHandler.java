package pingdata.io.pingdataandroidlibrary;

import android.graphics.Bitmap;
import android.media.Image;

import org.json.JSONObject;

import java.util.List;

/**
 * Interface for handling ping data uplifter results (which may run on a separate thread to the main thread.)
 */
public interface PingDataUplifterHandler {

    /**
     * Callback called when the receiptData has been uplifted.
     * @param receiptData Uplifted receipt data.
     */
    void handleUpliftedReceiptData(JSONObject receiptData);

    /**
     * Callback called when receipt images have been stitched
     * @param images The input receipt images.
     * @param stitchedImage The stitched image.
     */
    void handleReceiptImageData(List<Bitmap> images, Bitmap stitchedImage);

    /**
     * Handle error in uplifting receipt data.
     */
    void handleErrorInUplifting(PingDataUplifterError error);

    /**
     * Handle error in image stitching.
     */
    void handleErrorInImageStitching(PingDataUplifterError error);
}
