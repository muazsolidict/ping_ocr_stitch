package pingdata.io.pingdataandroidlibrary;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.media.ImageReader;
import android.os.Handler;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

class PingDataUplifterNative {
    private transient long nativePtr;
    protected transient boolean nativeMemOwn;

    protected PingDataUplifterNative(long cPtr, boolean cMemoryOwn) {
        nativeMemOwn = cMemoryOwn;
        nativePtr = cPtr;
    }

    protected static long getNativePtr(PingDataUplifterNative obj) {
        return (obj == null) ? 0 : obj.nativePtr;
    }

    protected void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (nativePtr != 0) {
            if (nativeMemOwn) {
                nativeMemOwn = false;
                PingDataUplifterJNI.deletePingDataUplifter(nativePtr);
            }
            nativePtr = 0;
        }
    }

    public String stringFromJNI() {
        return PingDataUplifterJNI.stringFromJNI(nativePtr);
    }

    public void addBitmapToStitch(Bitmap bitmap) throws java.lang.Exception {
        PingDataUplifterJNI.addBitmapToStitch(nativePtr, bitmap);
    }

    public int stitch() throws java.lang.Exception {
        return PingDataUplifterJNI.stitch();
    }

    public Bitmap stitchedImage() {
        return (Bitmap) PingDataUplifterJNI.stitchedImage();
    }

    public PingDataUplifterNative() {
        this(PingDataUplifterJNI.newPingDataUplifter(), true);
    }

}

public class PingDataUplifter implements Runnable {

    private final Context context;

    private final PingDataUplifterHandler handler;

    private final JSONObject receiptData;

    private List<Bitmap> receiptImages = new ArrayList<Bitmap>();

    private Bitmap stitchedImage;

    private final PingDataUplifterNative nativeUplifter = new PingDataUplifterNative();

    private final ReceiptDataRunnable receiptDataRunnable = new ReceiptDataRunnable(this);

    private final ReceiptImagesRunnable receiptImagesRunnable = new ReceiptImagesRunnable(this);

    /**
     * Constructor that takes in a list of images.
     * @param context Android application context.
     * @param handler Uplifter handler to handle the result of the uplifter.
     * @param receiptData Initial receipt data as a JSON blob.
     */
    public PingDataUplifter(Context context, PingDataUplifterHandler handler, JSONObject receiptData) {
        this.context = context;
        this.handler = handler;
        this.receiptData = receiptData;
        this.stitchedImage = null;
    }

    /**
     * Add a receipt image to the uplifter.
     * @param image An android image.
     */
    void addReceiptImage(Bitmap image) {
        this.receiptImages.add(image);
    }

    /**
     * Add a receipt image to the uplifter.
     * @param imagePath A path to an image on the phone.
     */
    void addReceiptImagePath(String imagePath) {

    }

    @Override
    public void run() {
        Handler mainHandler = new Handler(context.getMainLooper());
        mainHandler.post(receiptDataRunnable);
        for (Bitmap bitmap : this.receiptImages) {
            try {
                this.nativeUplifter.addBitmapToStitch(bitmap);
            } catch (Exception e) {
                mainHandler.post(new ReceiptStitchingErrorRunnable(this, new PingDataUplifterError(e.getMessage())));
            }
        }
        mainHandler.post(receiptImagesRunnable);
    }

    private class ReceiptDataRunnable implements Runnable {
        private PingDataUplifter uplifter;
        ReceiptDataRunnable(PingDataUplifter uplifter) {
            this.uplifter = uplifter;
        }
        @Override
        public void run() {
            this.uplifter.handler.handleUpliftedReceiptData(this.uplifter.receiptData);
        }
    }

    private class ReceiptImagesRunnable implements Runnable {
        private PingDataUplifter uplifter;
        ReceiptImagesRunnable(PingDataUplifter uplifter) {
            this.uplifter = uplifter;
        }
        @Override
        public void run() {
            this.uplifter.handler.handleReceiptImageData(this.uplifter.receiptImages, this.uplifter.stitchedImage);
        }
    }

    private class ReceiptStitchingErrorRunnable implements Runnable {
        private PingDataUplifter uplifter;
        private PingDataUplifterError error;
        ReceiptStitchingErrorRunnable(PingDataUplifter uplifter, PingDataUplifterError uplifterError) {
            this.uplifter = uplifter;
            this.error = uplifterError;
        }
        @Override
        public void run() {
            this.uplifter.handler.handleErrorInImageStitching(this.error);
        }
    }

    /* this is used to load the 'ping-data-android-sdk' library on application
     * startup.
     */
    static {
        System.loadLibrary("ping-data-android-sdk");
    }
}
