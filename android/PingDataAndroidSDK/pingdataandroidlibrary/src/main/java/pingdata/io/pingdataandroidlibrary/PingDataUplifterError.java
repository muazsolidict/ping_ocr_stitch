package pingdata.io.pingdataandroidlibrary;

/**
 * Created by asridhar on 31/5/18.
 */

public class PingDataUplifterError extends Exception {

    public PingDataUplifterError(String message) {
        super(message);
    }
    @Override
    public String getMessage() {
        return "PingDataUplifterError: " + super.getMessage();
    }
}
