package pingdata.io.pingdataandroidsdk;

import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import pingdata.io.pingdataandroidlibrary.PingDataUplifter;
import pingdata.io.pingdataandroidlibrary.PingDataUplifterHandler;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Example of a call to a native method
        TextView tv = (TextView) findViewById(R.id.sample_text);
        PingDataUplifter uplifter = new PingDataUplifter(new PingDataUplifterHandler() {
            @Override
            public void handleUpliftedReceiptData(JSONObject receiptData) {

            }

            @Override
            public void handleReceiptImageData(List<Image> images, Image stitchedImage) {

            }
        }, new JSONObject(), new ArrayList<Image>());
//        String uplifterText = uplifter.stringFromJNI();
//        tv.setText(uplifterText);

    }
}
