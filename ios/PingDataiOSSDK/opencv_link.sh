#!/bin/sh

set -x
set -euo pipefail

# Specify the path to the opencv2.framework file as the first argument to this script.
OPENCV_FRAMEWORK_PATH=$1

if [! -e $OPENCV_FRAMEWORK_PATH/Versions/Current/opencv2 ]
then
    echo "Could not find opencv2 static binary in framework"
    exit 1
else
    echo "Found opencv2 static binary"
fi

if [! -e $OPENCV_FRAMEWORK_PATH/Headers/stitching.hpp ]
then
    echo "Could not find opencv2 stitching header in framework"
    exit 1
else
    echo "Found opencv2 static binary"
fi

mkdir -p "Frameworks"
ln -s $OPENCV_FRAMEWORK_PATH "Frameworks/opencv2.framework"
