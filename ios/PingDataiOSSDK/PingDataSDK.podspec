Pod::Spec.new do |s|
    s.name              = 'PingDataSDK'
    s.version           = '1.0.4'
    s.summary           = 'PingData Uplifting SDK.'
    s.homepage          = 'http://pingdata.io/'

    s.author            = { 'Name' => 'anu@helixta.com.au' }
    s.license           = { :type => 'BSD', :file => 'LICENSE' }

    s.platform          = :ios
    s.source            = { :git => 'https://bitbucket.org/anuraagsridhar/pingdatasdk_ios.git', :tag => "#{s.version}" }

    s.ios.deployment_target = '9.0'
    s.ios.vendored_frameworks = 'PingDataiOSSDK.framework'
end
