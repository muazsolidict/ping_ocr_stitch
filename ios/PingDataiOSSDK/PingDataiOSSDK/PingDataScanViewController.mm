//
//  PingDataScanViewController.m
//  PingDataSDK
//
//  Created by Anuraag Sridhar on 9/5/18.
//  Copyright © 2018 Anuraag Sridhar. All rights reserved.
//

#import "PingDataScanViewController.h"
#import "OpenCV.h"
#import "PingDataUplifter.h"
#import "UIImage+OpenCV.h"

@interface PingDataScanViewController ()

@end

@implementation PingDataScanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.cameraQueue = dispatch_queue_create("camera queue", nil);
    self.captureSession = [[AVCaptureSession alloc] init];
    self.context = [[CIContext alloc] init];
    self.uplifter = [[PingDataUplifter alloc] init];
    [self checkPermission];
    dispatch_async(self.cameraQueue, ^{
        [self configureSession];
        [self.captureSession startRunning];
    });
    self.frameCount = 1;
    self.maxVariance = -1.0f;
    self.bestImage = Nil;
    self.lastFrame = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) checkPermission {
    switch ([AVCaptureDevice authorizationStatusForMediaType: AVMediaTypeVideo]) {
        case AVAuthorizationStatusAuthorized:
            self.permissionGranted = true;
        case AVAuthorizationStatusNotDetermined:
            [self requestPermission];
            break;
        default:
            self.permissionGranted = false;
            break;
    }
}

- (void) requestPermission {
    dispatch_suspend(self.cameraQueue);
    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
        self.permissionGranted = granted;
        dispatch_resume(self.cameraQueue);
    }];
}

- (void) configureSession {
    if (!self.permissionGranted) {
        return;
    }
    self.captureSession.sessionPreset = AVCaptureSessionPreset640x480;
    AVCaptureDevice* captureDevice = [self selectCaptureDevice];
    NSError* error;
    AVCaptureDeviceInput* captureDeviceInput = [[AVCaptureDeviceInput alloc] initWithDevice:captureDevice error:&error];
    if ([self.captureSession canAddInput:captureDeviceInput]) {
        [self.captureSession addInput:captureDeviceInput];
        AVCaptureVideoDataOutput* captureVideoOutput = [[AVCaptureVideoDataOutput alloc] init];
        [captureVideoOutput setVideoSettings:@{
                                             (NSString *)kCVPixelBufferPixelFormatTypeKey : @(kCVPixelFormatType_32BGRA),
                                             // Flag to enable emitting openGL frames from camera.
                                             (NSString *)kCVPixelBufferOpenGLESCompatibilityKey : @(YES)
                                             }];
        [captureVideoOutput setSampleBufferDelegate:self queue:dispatch_queue_create("sample buffer", nil)];
        if ([self.captureSession canAddOutput:captureVideoOutput]) {
            [self.captureSession addOutput:captureVideoOutput];
            AVCaptureConnection* connection = [captureVideoOutput connectionWithMediaType:AVMediaTypeVideo];
            if (connection.isVideoOrientationSupported) {
                connection.videoOrientation = AVCaptureVideoOrientationPortrait;
            }
        }
    }
}

- (AVCaptureDevice*) selectCaptureDevice {
    return [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
}

- (UIImage*) imageFromSampleBuffer: (CMSampleBufferRef)sampleBuffer {
    // Get a CMSampleBuffer's Core Video image buffer for the media data
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    
    // Lock the base address of the pixel buffer
    CVPixelBufferLockBaseAddress(imageBuffer, 0);
    
    // Get the number of bytes per row for the pixel buffer
    void *baseAddress = CVPixelBufferGetBaseAddress(imageBuffer);
    
    // Get the number of bytes per row for the pixel buffer
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
    
    // Get the pixel buffer width and height
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    
//    NSLog(@"w: %zu h: %zu bytesPerRow:%zu", width, height, bytesPerRow);
    
    // Create a device-dependent RGB color space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    // Create a bitmap graphics context with the sample buffer data
    CGContextRef context = CGBitmapContextCreate(baseAddress,
                                                 width,
                                                 height,
                                                 8,
                                                 bytesPerRow,
                                                 colorSpace,
                                                 kCGBitmapByteOrder32Little
                                                 | kCGImageAlphaPremultipliedFirst);
    // Create a Quartz image from the pixel data in the bitmap graphics context
    CGImageRef quartzImage = CGBitmapContextCreateImage(context);
    // Unlock the pixel buffer
    CVPixelBufferUnlockBaseAddress(imageBuffer,0);
    
    // Free up the context and color space
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    // Create an image object from the Quartz image
    //UIImage *image = [UIImage imageWithCGImage:quartzImage];
    UIImage *image = [UIImage imageWithCGImage:quartzImage];
    
    // Release the Quartz image
    CGImageRelease(quartzImage);
    
    return (image);
}


// MARK: AVCaptureVideoDataOutputSampleBufferDelegate
- (void)captureOutput:(AVCaptureOutput *)output didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection {
    UIImage* uiImage = [self imageFromSampleBuffer: sampleBuffer];
    dispatch_async(dispatch_get_main_queue(), ^{
        //        self.cameraImageView.image = uiImage;
        self.cameraImageView.image = uiImage;
        
        if (self.scanning) {
        
            
            int kernel_size = 3;
            int scale = 1;
            int delta = 0;
            int ddepth = CV_64F;
            cv::Mat src = [uiImage CVMat3];
            cv::Mat dst;
            cv::Laplacian(src, dst, ddepth, kernel_size, scale, delta, cv::BORDER_DEFAULT);
            std::vector<double> mean, stddev;
            cv::meanStdDev(dst, mean, stddev);
            double variance = stddev[0] * stddev[0];
            std::cout << "Variance " << variance << std::endl;
            
            if (variance > self.maxVariance) {
                self.maxVariance = variance;
                self.bestImage = uiImage;
            }
            
            if (self.frameCount % 15 == 0) {
                std::cout << "Frame " << self.frameCount << std::endl;
                double correl = 0.0f;
                if (self.lastImage.size.width > 0 && self.lastImage.size.height > 0) {
                    cv::Mat bestMat = [self.bestImage CVMat3];
                    cv::Mat im_float_1;
                    bestMat.convertTo(im_float_1, CV_8U);
                    cv::Mat lastMat = [self.lastImage CVMat3];
                    cv::Mat im_float_2;
                    lastMat.convertTo(im_float_2, CV_8U);
                    
                    int n_pixels = lastMat.rows * lastMat.cols;
                    
                    // Compute mean and standard deviation of both images
                    cv::Scalar im1_Mean, im1_Std, im2_Mean, im2_Std;
                    meanStdDev(im_float_1, im1_Mean, im1_Std);
                    meanStdDev(im_float_2, im2_Mean, im2_Std);
                    
                    // Compute covariance and correlation coefficient
                    double covar = (im_float_1 - im1_Mean).dot(im_float_2 - im2_Mean) / n_pixels;
                    correl = covar / (im1_Std[0] * im2_Std[0]);
                    std::cout << "Correlation " << correl << std::endl;
                }
                
                if (correl < 0.8 || self.frameCount - self.lastFrame > 20) {
                    std::cout << "Frame with highest variance " << self.maxVariance << std::endl;
                    [self.imageArray addObject:self.bestImage];
                    self.lastImage = self.bestImage;
                    self.lastFrame = self.frameCount;
                }
                self.maxVariance = -1.0f;
            }
            self.frameCount++;
        }
        NSString* imageCountString = [NSString stringWithFormat:@"Image Count: %ld", (unsigned long) self.imageArray.count];
        self.imageCountLabel.text = imageCountString;
    });
}
- (IBAction)cancelledScan:(id)sender {
    [self dismissViewControllerAnimated:true completion:^{
    }];
}

#define MAX_NUM_IMAGES 7.0

- (IBAction)holdingScan:(id)sender {
    self.scanInstructionsLabelContainer.hidden = YES;
    self.imageArray = [[NSMutableArray alloc] init];
    self.scanning = YES;
}
- (IBAction)releasedScan:(id)sender {
    self.scanning = NO;
    if (self.imageArray.count == 0) {
        self.scanInstructionsLabelContainer.hidden = NO;
        self.imageArray = nil;
        return;
    }
    if (self.delegate != nil) {
        [self displaySpinnerOnView: self.cameraImageView];
        if (self.maxVariance >= 0) {
            if (self.frameCount - self.lastFrame > 15) {
                std::cout << "Appending last frame" << std::endl;
                [self.imageArray addObject:self.bestImage];
            }
        }
        dispatch_async(self.cameraQueue, ^{
            UIImage* stitched = [self.uplifter stitchImages:self.imageArray];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.delegate finishedProcessingImages:self.imageArray withResult:stitched];
                [self dismissViewControllerAnimated:true completion:^{
                }];
            });
        });
    }
}

- (UIView*)displaySpinnerOnView: (UIView*) view {
    UIView* spinnerView = [[UIView alloc] initWithFrame:view.bounds];
    spinnerView.backgroundColor = [[UIColor alloc] initWithRed:0.5 green:0.5 blue:0.5 alpha:0.5];
    UIActivityIndicatorView* activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleWhiteLarge];
    [activityIndicator startAnimating];
    activityIndicator.center = spinnerView.center;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [spinnerView addSubview:activityIndicator];
        [view addSubview: spinnerView];
    });
    return spinnerView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
