//
//  PingDataUplifter.h
//  PingDataSDK
//
//  Created by Anuraag Sridhar on 9/5/18.
//  Copyright © 2018 Anuraag Sridhar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PingDataUplifterDelegate.h"

@interface PingDataUplifter : NSObject

- (void) startScanningReceipt;
- (UIImage*) stitchImagesFromFilepaths:(NSArray*) imagePaths;
- (UIImage*) stitchImages:(NSArray*) images;

- (void) upliftReceiptData:(NSDictionary*) receiptData withImages:(NSArray*) images;

@property id <PingDataUplifterDelegate> delegate;

@property dispatch_queue_t backgroundWorker;

@end
