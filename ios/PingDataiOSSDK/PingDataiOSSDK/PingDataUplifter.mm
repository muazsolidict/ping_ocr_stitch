//
//  PingDataUplifter.m
//  PingDataSDK
//
//  Created by Anuraag Sridhar on 9/5/18.
//  Copyright © 2018 Anuraag Sridhar. All rights reserved.
//

#import "PingDataUplifter.h"
#import "PingDataScanViewController.h"
#import "UIImage+Rotate.h"
#import "UIImage+OpenCV.h"
#import "PingDataStitcher.h"

@implementation PingDataUplifter

- (id) init {
    self = [super init];
    if (self != nil) {
        self.backgroundWorker = dispatch_queue_create("background worker thread", nil);
    }
    return self;
}

- (void) startScanningReceipt {
    NSLog(@"Starting stitching\n");
    NSString *frameworkBundleId = @"com.pingdata.ios.PingDataSDK";
    NSBundle *resourceBundle = [NSBundle bundleWithIdentifier:frameworkBundleId];
    NSLog(@"PingData SDK bundle: %@", [resourceBundle pathForResource:@"PingDataScanViewController" ofType:@"nib"]);
    
    PingDataScanViewController* viewController = [[PingDataScanViewController alloc] initWithNibName:@"PingDataScanViewController" bundle:resourceBundle];
    viewController.delegate = self.delegate;
    UIViewController* rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    [rootViewController presentViewController:viewController animated:YES completion:^() {}];
}

- (UIImage*) stitchImagesFromFilepaths:(NSArray*) imagePaths {
    NSMutableArray* images = [[NSMutableArray alloc] init];
    for (NSString* filepath in imagePaths) {
        UIImage* image = [UIImage imageWithContentsOfFile: filepath];
        [images addObject:image];
    }
    return [self stitchImages:images];
    
}

- (UIImage*) stitchImages:(NSMutableArray*) images {
    std::vector<cv::Mat> matImages;
    for (UIImage* image in images) {
        UIImage* rotatedImage = [image rotateToImageOrientation];
        cv::Mat matImage = [rotatedImage CVMat3];
        matImages.push_back(matImage);
    }
    //[images removeAllObjects];
    cv::Mat stitchedMat = pingdata::stitch(matImages);
    matImages.clear();
    UIImage* stitchedReceipt = [UIImage imageWithCVMat:stitchedMat];
    stitchedMat.release();
    return stitchedReceipt;
}

- (void) upliftReceiptData:(NSMutableDictionary*) receiptData withImages:(NSArray*) images {
    
    dispatch_async(self.backgroundWorker, ^{
        if (self.delegate != nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.delegate finishedLogoFingerprinting:receiptData];
            });
        }
        std::vector<cv::Mat> matImages;
        for (UIImage* image in images) {
            UIImage* rotatedImage = [image rotateToImageOrientation];
            cv::Mat matImage = [rotatedImage CVMat3];
            matImages.push_back(matImage);
        }
        cv::Mat stitchedMat = pingdata::stitch(matImages);
        matImages.clear();
        UIImage* stitchedReceipt = [UIImage imageWithCVMat:stitchedMat];
        stitchedMat.release();
        
        if (self.delegate != nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.delegate finishedProcessingImages:images withResult:stitchedReceipt];
            });
        }
    });

    
}

@end
