//
//  PingDataScanViewController.h
//  PingDataSDK
//
//  Created by Anuraag Sridhar on 9/5/18.
//  Copyright © 2018 Anuraag Sridhar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

#import "PingDataUplifterDelegate.h"
#import "PingDataUplifter.h"
#import "OpenCV.h"

@interface PingDataScanViewController : UIViewController <AVCaptureVideoDataOutputSampleBufferDelegate, UITableViewDelegate>


@property bool permissionGranted;

@property dispatch_queue_t cameraQueue;

@property AVCaptureSession* captureSession;

@property CIContext* context;

@property int frameCount;

@property NSMutableArray* imageArray;

@property BOOL scanning;

@property UIImage* stitchedImage;

@property double maxVariance;

@property UIImage* bestImage;

@property UIImage* lastImage;

@property int lastFrame;

@property (weak, nonatomic) IBOutlet UIImageView *cameraImageView;

@property (weak, nonatomic) IBOutlet UILabel *scanInstructionsLabel;
@property (weak, nonatomic) IBOutlet UIView *scanInstructionsLabelContainer;
@property (weak, nonatomic) IBOutlet UILabel *imageCountLabel;

@property PingDataUplifter* uplifter;
@property id <PingDataUplifterDelegate> delegate;

@end
