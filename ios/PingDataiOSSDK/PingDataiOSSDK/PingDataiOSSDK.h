//
//  PingDataiOSSDK.h
//  PingDataiOSSDK
//
//  Created by Anuraag Sridhar on 28/5/18.
//  Copyright © 2018 Anuraag Sridhar. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for PingDataiOSSDK.
FOUNDATION_EXPORT double PingDataiOSSDKVersionNumber;

//! Project version string for PingDataiOSSDK.
FOUNDATION_EXPORT const unsigned char PingDataiOSSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PingDataiOSSDK/PublicHeader.h>
#import <PingDataiOSSDK/PingDataUplifter.h>
#import <PingDataiOSSDK/PingDataUplifterDelegate.h>


