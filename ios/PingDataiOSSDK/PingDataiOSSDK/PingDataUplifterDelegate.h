//
//  PingDataUplifterDelegate.h
//  PingDataSDK
//
//  Created by Anuraag Sridhar on 9/5/18.
//  Copyright © 2018 Anuraag Sridhar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol PingDataUplifterDelegate <NSObject>

/**
 * Delegate methoid called upon logo fingerprinting completed to return
 * the uplifted data.
 */
- (void) finishedLogoFingerprinting:(NSMutableDictionary*) receiptInfo;

/**
 * Delegate method called upon stitching images completed to return stitched receipt.
 */
- (void) finishedProcessingImages: (NSArray*) images withResult: (UIImage*) image;

@end
