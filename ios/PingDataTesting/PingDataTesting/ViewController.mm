//
//  ViewController.m
//  PingDataTesting
//
//  Created by Anuraag Sridhar on 29/5/18.
//  Copyright © 2018 Anuraag Sridhar. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [BRScanManager sharedManager].licenseKey = @"S2GYMKUE-DY7ICQ4K-7Y53IQQI-BSI3IUS4-XXFIB2JZ-T3KISXTS-QFB4UIBZ-S3EEUQO2";
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
