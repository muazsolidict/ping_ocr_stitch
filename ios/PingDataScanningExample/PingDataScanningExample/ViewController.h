//
//  ViewController.h
//  PingDataScanningExample
//
//  Created by Anuraag Sridhar on 28/5/18.
//  Copyright © 2018 Anuraag Sridhar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BlinkReceipt/BlinkReceipt.h>
#import "PingDataiOSSDK/PingDataiOSSDK.h"

@interface ViewController : UIViewController <BRScanResultsDelegate, PingDataUplifterDelegate, UIScrollViewDelegate>

@property NSMutableArray* images;
@property UIImage* stitchedImage;
@property NSDictionary* receiptData;

@property (weak, nonatomic) IBOutlet UIButton *showReceiptImageButton;
@property (weak, nonatomic) IBOutlet UIButton *showReceiptDataButton;

@property PingDataUplifter* pingDataUplifter;

@end

