//
//  ReceiptImageViewController.m
//  PingDataScanningExample
//
//  Created by Anuraag Sridhar on 29/5/18.
//  Copyright © 2018 Anuraag Sridhar. All rights reserved.
//

#import "ReceiptImageViewController.h"

@interface ReceiptImageViewController ()

@end

@implementation ReceiptImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.receiptImageView.image = self.image;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
