//
//  ViewController.m
//  PingDataScanningExample
//
//  Created by Anuraag Sridhar on 28/5/18.
//  Copyright © 2018 Anuraag Sridhar. All rights reserved.
//

#import "ViewController.h"

#import "ReceiptImageViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.pingDataUplifter = [[PingDataUplifter alloc] init];
    self.pingDataUplifter.delegate = self;
    [BRScanManager sharedManager].licenseKey = @"S2GYMKUE-DY7ICQ4K-7Y53IQQI-BSI3IUS4-XXFIB2JZ-T3KISXTS-QFB4UIBZ-S3EEUQO2";
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)startScanning:(id)sender {
    BRScanOptions *scanOptions = [BRScanOptions new];
    scanOptions.storeUserFrames = YES;
    self.showReceiptDataButton.hidden = YES;
    self.showReceiptImageButton.hidden = YES;
    
    [[BRScanManager sharedManager] startStaticCameraFromController:self
                                                       scanOptions:scanOptions
                                                      withDelegate:self];
}

- (void)didFinishScanning:(UIViewController *)cameraViewController withScanResults:(BRScanResults *)scanResults {
    [cameraViewController dismissViewControllerAnimated:YES completion:nil];
    
    NSArray* imageFilepaths = ((BRScanManager*) [BRScanManager sharedManager]).userFramesFilepaths;
    self.images = [[NSMutableArray alloc] init];
    for (NSString* filepath in imageFilepaths) {
        UIImage* image = [UIImage imageWithContentsOfFile: filepath];
        [self.images addObject:image];
    }
    
    NSMutableDictionary* resultsDictionary = [[NSMutableDictionary alloc] init];
    [resultsDictionary setValue:@"test" forKey:@"test"];
    
    [self.pingDataUplifter upliftReceiptData:resultsDictionary withImages:self.images];
}

- (void)didCancelScanning:(UIViewController*)cameraViewController {
}

- (IBAction)showReceiptImage:(id)sender {
    ReceiptImageViewController* receiptImageViewController = [[ReceiptImageViewController alloc] init];
    receiptImageViewController.image = self.stitchedImage;
    [self presentViewController:receiptImageViewController animated:YES completion:^{
    }];
}

- (void)finishedLogoFingerprinting:(NSDictionary *)receiptInfo {
    self.receiptData = receiptInfo;
    self.showReceiptDataButton.hidden = NO;
}

- (void)finishedProcessingImages:(NSMutableArray *)images withResult:(UIImage *)image {
    self.stitchedImage = image;
    self.showReceiptImageButton.hidden = NO;
}

@end
