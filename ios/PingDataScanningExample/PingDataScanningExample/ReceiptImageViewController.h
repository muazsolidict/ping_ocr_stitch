//
//  ReceiptImageViewController.h
//  PingDataScanningExample
//
//  Created by Anuraag Sridhar on 29/5/18.
//  Copyright © 2018 Anuraag Sridhar. All rights reserved.
//

#import "ViewController.h"

@interface ReceiptImageViewController : ViewController
@property (weak, nonatomic) IBOutlet UIScrollView *receiptImageScrollView;
@property (weak, nonatomic) IBOutlet UIImageView *receiptImageView;
@property UIImage* image;
@end
