//
//  AppDelegate.h
//  PingDataScanningExample
//
//  Created by Anuraag Sridhar on 28/5/18.
//  Copyright © 2018 Anuraag Sridhar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

