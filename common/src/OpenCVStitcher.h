//
//  Stitcher.hpp
//  PingDataSDK
//
//  Created by Anuraag Sridhar on 26/4/18.
//  Copyright © 2018 Anuraag Sridhar. All rights reserved.
//

#ifndef PingDataStitcher_h
#define PingDataStitcher_h

#include "OpenCV.h"
#include <stdio.h>


class OpenCVStitcher
{
public:
    enum { ORIG_RESOL = -1 };
    enum Status
    {
        OK = 0,
        ERR_NEED_MORE_IMGS = 1,
        ERR_HOMOGRAPHY_EST_FAIL = 2,
        ERR_CAMERA_PARAMS_ADJUST_FAIL = 3
    };

    /**
     * @brief Creates a stitcher with the default parameters.
     * @return Stitcher class instance.
     */
    static OpenCVStitcher createDefault();
    /** 
     * @brief Creates a Stitcher configured in one of the stitching modes.
     * @return Stitcher class instance.
     */
    static cv::Ptr<OpenCVStitcher> create();

    double registrationResol() const { return m_registrationResolution; }
    void setRegistrationResol(double resol_mpx) { m_registrationResolution = resol_mpx; }

    double seamEstimationResol() const { return seam_est_resol_; }
    void setSeamEstimationResol(double resol_mpx) { seam_est_resol_ = resol_mpx; }

    double compositingResol() const { return compose_resol_; }
    void setCompositingResol(double resol_mpx) { compose_resol_ = resol_mpx; }

    double panoConfidenceThresh() const { return conf_thresh_; }
    void setPanoConfidenceThresh(double conf_thresh) { conf_thresh_ = conf_thresh; }

    bool waveCorrection() const { return do_wave_correct_; }
    void setWaveCorrection(bool flag) { do_wave_correct_ = flag; }

    cv::detail::WaveCorrectKind waveCorrectKind() const { return wave_correct_kind_; }
    void setWaveCorrectKind(cv::detail::WaveCorrectKind kind) { wave_correct_kind_ = kind; }

    cv::Ptr<cv::detail::FeaturesFinder> featuresFinder() { return m_featuresFinder; }
    const cv::Ptr<cv::detail::FeaturesFinder> featuresFinder() const { return m_featuresFinder; }
    void setFeaturesFinder(cv::Ptr<cv::detail::FeaturesFinder> featuresFinder)
        { m_featuresFinder = featuresFinder; }

    cv::Ptr<cv::detail::FeaturesMatcher> featuresMatcher() { return m_featuresMatcher; }
    const cv::Ptr<cv::detail::FeaturesMatcher> featuresMatcher() const { return m_featuresMatcher; }
    void setFeaturesMatcher(cv::Ptr<cv::detail::FeaturesMatcher> features_matcher)
        { m_featuresMatcher = features_matcher; }

    const cv::UMat& matchingMask() const { return matching_mask_; }
    void setMatchingMask(const cv::UMat &mask)
    {
        CV_Assert(mask.type() == CV_8U && mask.cols == mask.rows);
        matching_mask_ = mask.clone();
    }

    cv::Ptr<cv::detail::BundleAdjusterBase> bundleAdjuster() { return bundle_adjuster_; }
    const cv::Ptr<cv::detail::BundleAdjusterBase> bundleAdjuster() const { return bundle_adjuster_; }
    void setBundleAdjuster(cv::Ptr<cv::detail::BundleAdjusterBase> bundle_adjuster)
        { bundle_adjuster_ = bundle_adjuster; }

    /* TODO OpenCV ABI 4.x
       cv::Ptr<cv::detail::Estimator> estimator() { return estimator_; }
    const cv::Ptr<cv::detail::Estimator> estimator() const { return estimator_; }
    void setEstimator(cv::Ptr<cv::detail::Estimator> estimator)
        { estimator_ = estimator; }
    */

    cv::Ptr<cv::WarperCreator> warper() { return warper_; }
    const cv::Ptr<cv::WarperCreator> warper() const { return warper_; }
    void setWarper(cv::Ptr<cv::WarperCreator> creator) { warper_ = creator; }

    cv::Ptr<cv::detail::ExposureCompensator> exposureCompensator() { return exposure_comp_; }
    const cv::Ptr<cv::detail::ExposureCompensator> exposureCompensator() const { return exposure_comp_; }
    void setExposureCompensator(cv::Ptr<cv::detail::ExposureCompensator> exposure_comp)
        { exposure_comp_ = exposure_comp; }

    cv::Ptr<cv::detail::SeamFinder> seamFinder() { return seam_finder_; }
    const cv::Ptr<cv::detail::SeamFinder> seamFinder() const { return seam_finder_; }
    void setSeamFinder(cv::Ptr<cv::detail::SeamFinder> seam_finder) { seam_finder_ = seam_finder; }

    cv::Ptr<cv::detail::Blender> blender() { return blender_; }
    const cv::Ptr<cv::detail::Blender> blender() const { return blender_; }
    void setBlender(cv::Ptr<cv::detail::Blender> b) { blender_ = b; }

    /**
     * @brief These functions try to match the given images and to estimate rotations of each camera.
     * @note Use the functions only if you're aware of the stitching pipeline, otherwise use Stitcher::stitch.
     * @param images Input images.
     * @return Status code.
     */
    Status estimateTransform(cv::InputArrayOfArrays images);

    /** @overload */
    Status composePanorama(cv::OutputArray pano);
    /** @brief These functions try to compose the given images (or images stored internally from the other function
    calls) into the final pano under the assumption that the image transformations were estimated
    before.

    @note Use the functions only if you're aware of the stitching pipeline, otherwise use
    Stitcher::stitch.

    @param images Input images.
    @param pano Final pano.
    @return Status code.
     */
    Status composePanorama(cv::InputArrayOfArrays images, cv::OutputArray pano);

    Status stitch(cv::InputArrayOfArrays images, cv::OutputArray pano);
    /**
     * @brief These functions try to stitch the given images.
     * @param images Input images.
     * @param rois Region of interest rectangles.
     * @param pano Final pano.
     * @return Status code.
     */
    Status stitch(cv::InputArrayOfArrays images, const std::vector<std::vector<cv::Rect> > &rois, cv::OutputArray pano);

    std::vector<int> component() const { return m_indices; }
    std::vector<cv::detail::CameraParams> cameras() const { return cameras_; }
    double workScale() const { return m_workScale; }
    void setWorkScale(double workScale) { m_workScale = workScale; }

private:
    //Stitcher() {}
    // v2 code
    std::vector<cv::UMat> m_originalImages;
    std::vector<cv::Size> m_originalImageSizes;
    double m_registrationResolution;
    double m_workScale;

    // Feature finding.
    cv::Ptr<cv::detail::FeaturesFinder> m_featuresFinder;
    std::vector<cv::detail::ImageFeatures> m_features;

    // Feature matching.
    cv::Ptr<cv::detail::FeaturesMatcher> m_featuresMatcher;
    std::vector<cv::detail::MatchesInfo> pairwise_matches_;
    
    std::vector<int> m_indices;
    
    Status matchImages();
    Status estimateCameraParams();

    // end v2 code

    // v1 code
    double seam_est_resol_;
    double compose_resol_;
    double conf_thresh_;
    cv::UMat matching_mask_;
    cv::Ptr<cv::detail::BundleAdjusterBase> bundle_adjuster_;
    /* TODO OpenCV ABI 4.x
    cv::Ptr<cv::detail::Estimator> estimator_;
    */
    bool do_wave_correct_;
    cv::detail::WaveCorrectKind wave_correct_kind_;
    cv::Ptr<cv::WarperCreator> warper_;
    cv::Ptr<cv::detail::ExposureCompensator> exposure_comp_;
    cv::Ptr<cv::detail::SeamFinder> seam_finder_;
    cv::Ptr<cv::detail::Blender> blender_;

    std::vector<cv::UMat> seam_est_imgs_;
    std::vector<cv::detail::CameraParams> cameras_;
    double seam_scale_;
    double seam_work_aspect_;
    double warped_image_scale_;
};

#endif /* Stitcher_hpp */
