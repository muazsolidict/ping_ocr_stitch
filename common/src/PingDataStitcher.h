#ifndef stitcher_h
#define stitcher_h
#include "OpenCVStitcher.h"

namespace pingdata
{
/**
 * Main stitching function.
 * Wraps all the necessary steps for stitching.
 */
cv::Mat stitch (std::vector <cv::Mat> & images);
    
}
#endif
