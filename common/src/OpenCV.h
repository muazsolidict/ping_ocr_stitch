//
//  OpenCV.h
//  PingDataSDK
//
//  Created by Anuraag Sridhar on 26/4/18.
//  Copyright © 2018 Anuraag Sridhar. All rights reserved.
//

#ifndef OpenCV_h
#define OpenCV_h

#ifdef __cplusplus
#undef NO
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdocumentation"
#import <opencv2/opencv.hpp>
#pragma clang diagnostic pop
#define NO 0
#endif

#endif /* OpenCV_h */
