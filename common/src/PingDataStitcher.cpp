#include "PingDataStitcher.h"

using namespace std;

bool try_use_gpu = true;

namespace pingdata {
//static const float DOWNSAMPLE = 2.0f;

void preprocessImage(const cv::Mat& image, cv::Mat& finalImage)
{
    cv::Mat tmp1;
    cv::Mat kernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(5, 5));
    cv::erode(image, tmp1, kernel);
    cv::blur(tmp1, finalImage, cv::Size(5,5));
    tmp1.release();
}

bool updateImageMaskAndFill(cv::Mat& src, cv::Mat& dst, cv::Mat& mask, int x, int y) {
    cv::Vec3b& srcColor = src.at<cv::Vec3b>(cv::Point(x, y));
    if (srcColor[0] != 0 && srcColor[1] != 0 && srcColor[2] != 0) {
        return false;
    }
    cv::Vec3b& dstColor = dst.at<cv::Vec3b>(cv::Point(x, y));
    dstColor[0] = dstColor[1] = dstColor[2] = 208;
    mask.at<uchar>(cv::Point(x, y)) = 255;
    return true;
}

void backfillImage(cv::Mat& image) {
    int width = image.cols;
    int height = image.rows;
    cv::Mat toFill;
    toFill.create(height, width, CV_8UC3);
    toFill.setTo(cv::Scalar(255,255,255));
    cv::Mat mask;
    mask.create(height, width, CV_8UC1);
    mask.setTo(cv::Scalar(0, 0, 0));
    
    for (int y = 0; y < height; y++) {
        // Backfill from the right
        for (int x = width - 1; x >= 0; x--) {
            if (!updateImageMaskAndFill(image, toFill, mask, x, y))
            {
                break;
            }
        }
        
        // Backfill from the left
        for (int x = 0; x < width; x++) {
            if (!updateImageMaskAndFill(image, toFill, mask, x, y))
            {
                break;
            }
        }
    }
    
    for (int x = 0; x < width; x++) {
        // Backfill from the top
        for (int y = height - 1; y >= 0; y--) {
            if (!updateImageMaskAndFill(image, toFill, mask, x, y))
            {
                break;
            }
        }
        
        // Backfill from the left
        for (int y = 0; y < height; y++) {
            if (!updateImageMaskAndFill(image, toFill, mask, x, y))
            {
                break;
            }
        }
    }
    // Create a blurred background
    cv::Mat blurredBgnd = image.clone();
    toFill.copyTo(blurredBgnd, mask);
    toFill.release();
    cv::blur(blurredBgnd, blurredBgnd, cv::Size(100,100));
    // Overlay the blurred background onto the source image.
    blurredBgnd.copyTo(image, mask);
    mask.release();
    blurredBgnd.release();
}

void perforatedConcat(const cv::Mat& ongoingImage, const cv::Mat& newImage, cv::Mat& finalImage) {
    int threshold = 15;
    cv::Scalar perforationColor(192, 192, 192);
    int zig_zag_width = 50;
    int prevHeight = ongoingImage.rows;
    cv::vconcat(ongoingImage, newImage, finalImage);
    cv::rectangle(finalImage, cv::Point(0, prevHeight - threshold), cv::Point(finalImage.cols, prevHeight + threshold), perforationColor, cv::FILLED);
    for (int j = 0; j < (finalImage.cols + zig_zag_width); j+= zig_zag_width) {
        vector<cv::Point> trianglePoints(3);
        trianglePoints[0] = cv::Point(j, prevHeight - threshold);
        trianglePoints[1] = cv::Point(j + zig_zag_width / 2, prevHeight - threshold - zig_zag_width / 2);
        trianglePoints[2] = cv::Point(j + zig_zag_width, prevHeight - threshold);
        cv::fillConvexPoly(finalImage, trianglePoints, perforationColor, 8);
        
        trianglePoints[0] = cv::Point(j - zig_zag_width / 2, prevHeight + threshold);
        trianglePoints[1] = cv::Point(j, prevHeight + threshold + zig_zag_width / 2);
        trianglePoints[2] = cv::Point(j + zig_zag_width / 2, prevHeight + threshold);
        cv::fillConvexPoly(finalImage, trianglePoints, perforationColor, 8);
        
    }
}

void constructHorizonatalConcatenatedImage(vector<cv::Mat>& images, cv::Mat& finalImage) {
    finalImage = images[0].clone();
    for (size_t i = 1; i < images.size(); i++) {
//        vconcat(finalImage, images[i], finalImage);
        perforatedConcat(finalImage, images[i], finalImage);
    }
}

void compressImage(cv::Mat& image, vector<uchar>& buffer)
{
    vector<int> param(2);
    param[0] = cv::IMWRITE_JPEG_QUALITY;
    param[1] = 75;
    cv::imencode(".jpg", image, buffer, param);
}

void decompressImage(vector<uchar>& stream, cv::Mat& buffer, int flags)
{
    cv::imdecode(stream, flags, &buffer);
}

cv::Mat tryStitch (vector<cv::Mat>& images)
{
    cv::Mat pano;
    cout << "Now I'm stitching " << images.size() << " images" << endl;
    if (images.size() == 1)
    {
        // cout << "Only one image found, so return this image (as grayscale)" << endl;
        cv::Mat tmp;
        cv::cvtColor(images[0], tmp, cv::COLOR_BGR2GRAY);
        cv::cvtColor(tmp, pano, cv::COLOR_GRAY2BGR);
        tmp.release();
        images[0].release();
    }
    else
    {
        vector<vector<uchar>> compressedImages(images.size());
        // cout << "Compressing receipt images" << endl;
        for (size_t i = 0; i < images.size(); i++)
        {
            cv::Mat tmpGray;
            cv::cvtColor(images[i], tmpGray, cv::COLOR_BGR2GRAY);
            images[i].release();
            compressImage(tmpGray, compressedImages[i]);
            tmpGray.release();
        }
        // cout << "Converting images to grayscale and eroding to improve the success of a stitch" << endl;
        vector<cv::Mat> preprocessedImages(images.size());
        for (size_t i = 0; i < images.size(); i++)
        {
            cv::Mat tmpOriginal;
            //tmpDecimated, tmpGray;
            //resize(images[i], tmpDecimated, Size(), 0.5, 0.5);
            //images[i].release();
            decompressImage(compressedImages[i], tmpOriginal, cv::IMREAD_GRAYSCALE);
            preprocessImage(tmpOriginal, preprocessedImages[i]);
        }
        cout << "Erosion complete. Stitching." << endl;
        cv::Ptr<cv::Stitcher> stitcher = cv::Stitcher::create(cv::Stitcher::Mode::PANORAMA, try_use_gpu);
        stitcher->setWarper(cv::makePtr<cv::MercatorWarper>());
        stitcher->setWaveCorrection(true);
        stitcher->setWaveCorrectKind(cv::detail::WAVE_CORRECT_VERT);
        stitcher->setFeaturesMatcher(cv::makePtr<cv::detail::BestOf2NearestRangeMatcher>(2, try_use_gpu));
        stitcher->setCompositingResol(1.0f);
        stitcher->setRegistrationResol(0.3f);
        cv::Mat rotatedOngoingPanorama;
        int status = 0;
        try {
            status = stitcher->estimateTransform(preprocessedImages);
        } catch (const cv::Exception& e) {
            cout << "error in stitching images together: " << e.what() << endl;
            cout << "concatenating images instead" << endl;
            status = 0;
        }
        // Free the pre-processed images.
        preprocessedImages.clear();
        
        for (size_t i = 0; i < images.size(); i++) {
            decompressImage(compressedImages[i], images[i], cv::IMREAD_COLOR);
        }
        compressedImages.clear();
        bool success = true;
        if (status != cv::Stitcher::OK)
        {
            cout << "can't stitch images, error code = " << int(status) << endl;
            cout << "concatenating images instead" << endl;
            constructHorizonatalConcatenatedImage(images, rotatedOngoingPanorama);
            success = false;
        } else {
            std::vector<int> indices = stitcher->component();
            // cout << "Used " << indices.size() << " images for stitching" << endl;
            vector<cv::Mat> stitchableImages(indices.size());
            for (size_t i = 0; i < indices.size(); i++) {
                stitchableImages[i] = images[indices[i]];
            }
            for (size_t i = 0; i < indices.size(); i++) {
                stitchableImages[i] = images[indices[i]];
                std::cout << "Image " << i << " Image size: " << stitchableImages[i].cols << ", " << stitchableImages[i].rows << " - " << stitchableImages[i].type() << std::endl;
            }
            
            try {
                stitcher->composePanorama(stitchableImages, rotatedOngoingPanorama);
                backfillImage(rotatedOngoingPanorama);
                
                size_t idx = images.size();
                for (; idx > 0; idx--) {
                    size_t value = idx - 1;
                    if (std::find(indices.begin(), indices.end(), value) != indices.end()) {
                        break;
                    }
                    std::cout << "Excluded index " << value << std::endl;
                }
                for (size_t i = 0; i < idx; i++) {
                    std::cout << "Processed index " << i << std::endl;
                    images.erase(images.begin());
                }
                
            } catch (const cv::Exception& e) {
                cout << "error in stitching images together: " << e.what() << endl;
                cout << "concatenating images instead" << endl;
                constructHorizonatalConcatenatedImage(images, rotatedOngoingPanorama);
                success = false;
            }
        }
        stitcher.release();
        //images.clear();
        if (!success) {
            pano = rotatedOngoingPanorama.clone();
        } else {
            cv::rotate(rotatedOngoingPanorama, pano, cv::ROTATE_90_CLOCKWISE);
        }
        rotatedOngoingPanorama.release();
    }
    //cv::Mat erodedPano;
    //cv::erode(pano, erodedPano, cv::getStructuringElement(MORPH_RECT, cv::Size(1, 1)));
    //pano = erodedPano.clone();
    return pano;
}


cv::Mat stitch (vector<cv::Mat>& images) {
    cv::Mat final, pano;
    final.create(0, 0, CV_8UC1);
    size_t maxImages = images.size();
    size_t unStitched = maxImages;
    
    while (unStitched > 0) {
        pano = tryStitch(images);
        unStitched = images.size();
        if (final.rows == 0 && final.cols == 0) {
            final = pano.clone();
        } else {
            if (final.cols < pano.cols) {
                int half = (int)(pano.cols - final.cols) / 2;
                cv::copyMakeBorder(final, final, 0, 0, half, pano.cols - final.cols - half, cv::BORDER_CONSTANT, cv::Scalar(0, 0, 0));
                backfillImage(final);
            } else if (final.cols > pano.cols) {
                int half = (int)(final.cols - pano.cols) / 2;
                cv::copyMakeBorder(pano, pano, 0, 0, half, final.cols - pano.cols - half, cv::BORDER_CONSTANT, cv::Scalar(0, 0, 0));
                backfillImage(pano);
            }
            
//            cv::vconcat(final, pano, final);
            
            perforatedConcat(final, pano, final);
        }
        
        if (unStitched == maxImages) {
            break;
        }
        maxImages = unStitched;
    }
    return final;
}
    
}

