//
//  Stitcher.cpp
//  PingDataSDK
//
//  Created by Anuraag Sridhar on 26/4/18.
//  Copyright © 2018 Anuraag Sridhar. All rights reserved.
//

#include "OpenCVStitcher.h"
#include <map>
using namespace std;

#define MEASURE_PERFORMANCE 0

#if MEASURE_PERFORMANCE
std::map<std::string, int64> performanceCounters;
#endif

void startProfiling(const char* task) {
#if MEASURE_PERFORMANCE
    cout << "starting " << task << endl;
    performanceCounters[task] = cv::getTickCount();
#endif
}

void endProfiling(const char* task) {
#if MEASURE_PERFORMANCE
    if (performanceCounters.find(task) != performanceCounters.end()) {
        int64 startTick = performanceCounters[task];
        int64 endTick = cv::getTickCount();
        cout << task << " took " << (endTick - startTick) / cv::getTickFrequency() << " seconds" << endl;
    }
#endif
}


OpenCVStitcher OpenCVStitcher::createDefault()
{
    OpenCVStitcher stitcher;
    stitcher.setRegistrationResol(0.3);
    stitcher.setSeamEstimationResol(0.05);
    stitcher.setCompositingResol(ORIG_RESOL);
    stitcher.setPanoConfidenceThresh(1);
    stitcher.setWaveCorrection(true);
    stitcher.setWaveCorrectKind(cv::detail::WAVE_CORRECT_HORIZ);
    stitcher.setFeaturesMatcher(cv::makePtr<cv::detail::BestOf2NearestMatcher>());
    stitcher.setBundleAdjuster(cv::makePtr<cv::detail::BundleAdjusterRay>());

#ifdef HAVE_OPENCV_XFEATURES2D
    stitcher.setFeaturesFinder(cv::makePtr<cv::detail::SurfFeaturesFinder>());
#else
    stitcher.setFeaturesFinder(cv::makePtr<cv::detail::OrbFeaturesFinder>());
#endif
    stitcher.setWarper(cv::makePtr<cv::SphericalWarper>());
    stitcher.setSeamFinder(cv::makePtr<cv::detail::GraphCutSeamFinder>(cv::detail::GraphCutSeamFinderBase::COST_COLOR));

    stitcher.setExposureCompensator(cv::makePtr<cv::detail::BlocksGainCompensator>());
    stitcher.setBlender(cv::makePtr<cv::detail::MultiBandBlender>());

    stitcher.m_workScale = 1.0;
    stitcher.seam_scale_ = 1;
    stitcher.seam_work_aspect_ = 1;
    stitcher.warped_image_scale_ = 1;

    return stitcher;
}


cv::Ptr<OpenCVStitcher> OpenCVStitcher::create()
{
    OpenCVStitcher stit = createDefault();
    cv::Ptr<OpenCVStitcher> stitcher = cv::makePtr<OpenCVStitcher>(stit);
    return stitcher;
}


OpenCVStitcher::Status OpenCVStitcher::estimateTransform(cv::InputArrayOfArrays images)
{
    images.getUMatVector(m_originalImages);
    Status status;

    if ((status = matchImages()) != OK)
        return status;

    if ((status = estimateCameraParams()) != OK)
        return status;

    return OK;
}

OpenCVStitcher::Status OpenCVStitcher::matchImages()
{
    if (m_originalImages.size() < 2)
    {
        return ERR_NEED_MORE_IMGS;
    }

    m_workScale = 1.0;
    seam_work_aspect_ = 1;
    seam_scale_ = 1;
    bool isWorkScaleSet = false;
    bool is_seam_scale_set = false;
    cv::UMat full_img, img;
    m_features.resize(m_originalImages.size());
    seam_est_imgs_.resize(m_originalImages.size());
    m_originalImageSizes.resize(m_originalImages.size());

    std::vector<cv::UMat> feature_find_imgs(m_originalImages.size());

    startProfiling("downsampling images");

    for (size_t i = 0; i < m_originalImages.size(); ++i)
    {
        full_img = m_originalImages[i];
        m_originalImageSizes[i] = full_img.size();
        
        if (m_registrationResolution < 0)
        {
            img = full_img;
            m_workScale = 1;
            isWorkScaleSet = true;
        }
        else
        {
            if (!isWorkScaleSet)
            {
                m_workScale = std::min(1.0, std::sqrt(m_registrationResolution * 1e6 / full_img.size().area()));
                isWorkScaleSet = true;
            }
            resize(full_img, img, cv::Size(), m_workScale, m_workScale, cv::INTER_LINEAR_EXACT);
        }
        if (!is_seam_scale_set)
        {
            seam_scale_ = std::min(1.0, std::sqrt(seam_est_resol_ * 1e6 / full_img.size().area()));
            seam_work_aspect_ = seam_scale_ / m_workScale;
            is_seam_scale_set = true;
        }
        feature_find_imgs[i] = img;
        m_features[i].img_idx = (int)i;

        resize(full_img, img, cv::Size(), seam_scale_, seam_scale_, cv::INTER_LINEAR_EXACT);
        seam_est_imgs_[i] = img.clone();
    }

    endProfiling("downsampling images");

    // find features possibly in parallel
    startProfiling("finding features");
    (*m_featuresFinder)(feature_find_imgs, m_features);
    endProfiling("finding features");

    // Do it to save memory
    m_featuresFinder->collectGarbage();
    full_img.release();
    img.release();
    feature_find_imgs.clear();

    startProfiling("matching features");
    (*m_featuresMatcher)(m_features, pairwise_matches_, matching_mask_);
    m_featuresMatcher->collectGarbage();
    endProfiling("matching features");

    // Leave only images we are sure are from the same panorama
    startProfiling("filtering images");
    m_indices = cv::detail::leaveBiggestComponent(m_features, pairwise_matches_, (float)conf_thresh_);
    std::vector<cv::UMat> seam_est_imgs_subset;
    std::vector<cv::UMat> imgs_subset;
    std::vector<cv::Size> full_img_sizes_subset;
    for (size_t i = 0; i < m_indices.size(); ++i)
    {
        imgs_subset.push_back(m_originalImages[m_indices[i]]);
        seam_est_imgs_subset.push_back(seam_est_imgs_[m_indices[i]]);
        full_img_sizes_subset.push_back(m_originalImageSizes[m_indices[i]]);
    }
    seam_est_imgs_ = seam_est_imgs_subset;
    m_originalImages = imgs_subset;
    m_originalImageSizes = full_img_sizes_subset;
    endProfiling("filtering images");

    if (m_originalImages.size() < 2)
    {
        return ERR_NEED_MORE_IMGS;
    }

    return OK;
}

OpenCVStitcher::Status OpenCVStitcher::composePanorama(cv::OutputArray pano)
{
    return composePanorama(std::vector<cv::UMat>(), pano);
}

OpenCVStitcher::Status OpenCVStitcher::composePanorama(cv::InputArrayOfArrays images, cv::OutputArray pano)
{
    std::vector<cv::UMat> imgs;
    images.getUMatVector(imgs);

    startProfiling("downsampling stitched images");
    if (!imgs.empty())
    {
        CV_Assert(imgs.size() == m_originalImages.size());

        cv::UMat img;
        seam_est_imgs_.resize(imgs.size());

        for (size_t i = 0; i < imgs.size(); ++i)
        {
            m_originalImages[i] = imgs[i];
            resize(imgs[i], img, cv::Size(), seam_scale_, seam_scale_, cv::INTER_LINEAR_EXACT);
            seam_est_imgs_[i] = img.clone();
        }

        std::vector<cv::UMat> seam_est_imgs_subset;
        std::vector<cv::UMat> imgs_subset;

        for (size_t i = 0; i < m_indices.size(); ++i)
        {
            imgs_subset.push_back(m_originalImages[m_indices[i]]);
            seam_est_imgs_subset.push_back(seam_est_imgs_[m_indices[i]]);
        }

        seam_est_imgs_ = seam_est_imgs_subset;
        m_originalImages = imgs_subset;
    }
    endProfiling("downsampling stitched images");

    cv::UMat pano_;

    std::vector<cv::Point> corners(m_originalImages.size());
    std::vector<cv::UMat> masks_warped(m_originalImages.size());
    std::vector<cv::UMat> images_warped(m_originalImages.size());
    std::vector<cv::Size> sizes(m_originalImages.size());
    std::vector<cv::UMat> masks(m_originalImages.size());

    // Prepare image masks
    startProfiling("preparing image masks");
    for (size_t i = 0; i < m_originalImages.size(); ++i)
    {
        masks[i].create(seam_est_imgs_[i].size(), CV_8U);
        masks[i].setTo(cv::Scalar::all(255));
    }
    endProfiling("preparing image masks");

    startProfiling("warping images");
    // Warp images and their masks
    cv::Ptr<cv::detail::RotationWarper> w = warper_->create(float(warped_image_scale_ * seam_work_aspect_));
    for (size_t i = 0; i < m_originalImages.size(); ++i)
    {
        cv::Mat_<float> K;
        cameras_[i].K().convertTo(K, CV_32F);
        K(0,0) *= (float)seam_work_aspect_;
        K(0,2) *= (float)seam_work_aspect_;
        K(1,1) *= (float)seam_work_aspect_;
        K(1,2) *= (float)seam_work_aspect_;

        corners[i] = w->warp(seam_est_imgs_[i], K, cameras_[i].R, cv::INTER_LINEAR, cv::BORDER_REFLECT, images_warped[i]);
        sizes[i] = images_warped[i].size();

        w->warp(masks[i], K, cameras_[i].R, cv::INTER_NEAREST, cv::BORDER_CONSTANT, masks_warped[i]);
    }
    endProfiling("warping images");


    startProfiling("exposure compensation");
    // Compensate exposure before finding seams
    exposure_comp_->feed(corners, images_warped, masks_warped);
    for (size_t i = 0; i < m_originalImages.size(); ++i)
        exposure_comp_->apply(int(i), corners[i], images_warped[i], masks_warped[i]);

    endProfiling("exposure compensation");

    // Find seams
    startProfiling("finding seams");
    std::vector<cv::UMat> images_warped_f(m_originalImages.size());
    for (size_t i = 0; i < m_originalImages.size(); ++i)
        images_warped[i].convertTo(images_warped_f[i], CV_32F);
    seam_finder_->find(images_warped_f, corners, masks_warped);
    endProfiling("finding seams");

    // Release unused memory
    seam_est_imgs_.clear();
    images_warped.clear();
    images_warped_f.clear();
    masks.clear();

    cv::UMat img_warped, img_warped_s;
    cv::UMat dilated_mask, seam_mask, mask, mask_warped;

    //double compose_seam_aspect = 1;
    double compose_work_aspect = 1;
    bool is_blender_prepared = false;

    double compose_scale = 1;
    bool is_compose_scale_set = false;

    std::vector<cv::detail::CameraParams> cameras_scaled(cameras_);

    startProfiling("blending images");
    cv::UMat full_img, img;
    for (size_t img_idx = 0; img_idx < m_originalImages.size(); ++img_idx)
    {
        // Read image and resize it if necessary
        full_img = m_originalImages[img_idx];
        if (!is_compose_scale_set)
        {
            if (compose_resol_ > 0)
                compose_scale = std::min(1.0, std::sqrt(compose_resol_ * 1e6 / full_img.size().area()));
            is_compose_scale_set = true;

            // Compute relative scales
            //compose_seam_aspect = compose_scale / seam_scale_;
            compose_work_aspect = compose_scale / m_workScale;

            // Update warped image scale
            float warp_scale = static_cast<float>(warped_image_scale_ * compose_work_aspect);
            w = warper_->create(warp_scale);

            // Update corners and sizes
            for (size_t i = 0; i < m_originalImages.size(); ++i)
            {
                // Update intrinsics
                cameras_scaled[i].ppx *= compose_work_aspect;
                cameras_scaled[i].ppy *= compose_work_aspect;
                cameras_scaled[i].focal *= compose_work_aspect;

                // Update corner and size
                cv::Size sz = m_originalImageSizes[i];
                if (std::abs(compose_scale - 1) > 1e-1)
                {
                    sz.width = cvRound(m_originalImageSizes[i].width * compose_scale);
                    sz.height = cvRound(m_originalImageSizes[i].height * compose_scale);
                }

                cv::Mat K;
                cameras_scaled[i].K().convertTo(K, CV_32F);
                cv::Rect roi = w->warpRoi(sz, K, cameras_scaled[i].R);
                corners[i] = roi.tl();
                sizes[i] = roi.size();
            }
        }
        if (std::abs(compose_scale - 1) > 1e-1)
        {
            resize(full_img, img, cv::Size(), compose_scale, compose_scale, cv::INTER_LINEAR_EXACT);
        }
        else
            img = full_img;
        full_img.release();
        cv::Size img_size = img.size();


        cv::Mat K;
        cameras_scaled[img_idx].K().convertTo(K, CV_32F);

        // Warp the current image
        w->warp(img, K, cameras_[img_idx].R, cv::INTER_LINEAR, cv::BORDER_REFLECT, img_warped);

        // Warp the current image mask
        mask.create(img_size, CV_8U);
        mask.setTo(cv::Scalar::all(255));
        w->warp(mask, K, cameras_[img_idx].R, cv::INTER_NEAREST, cv::BORDER_CONSTANT, mask_warped);

        // Compensate exposure
        exposure_comp_->apply((int)img_idx, corners[img_idx], img_warped, mask_warped);

        img_warped.convertTo(img_warped_s, CV_16S);
        img_warped.release();
        img.release();
        mask.release();

        // Make sure seam mask has proper size
        dilate(masks_warped[img_idx], dilated_mask, cv::Mat());
        resize(dilated_mask, seam_mask, mask_warped.size(), 0, 0, cv::INTER_LINEAR_EXACT);

        bitwise_and(seam_mask, mask_warped, mask_warped);


        if (!is_blender_prepared)
        {
            blender_->prepare(corners, sizes);
            is_blender_prepared = true;
        }
        // Blend the current image
        blender_->feed(img_warped_s, mask_warped, corners[img_idx]);
    }

    cv::UMat result, result_mask;
    blender_->blend(result, result_mask);
    endProfiling("blending images");


    // Preliminary result is in CV_16SC3 format, but all values are in [0,255] range,
    // so convert it to avoid user confusing
    result.convertTo(pano, CV_8U);

    return OK;
}


OpenCVStitcher::Status OpenCVStitcher::stitch(cv::InputArrayOfArrays images, cv::OutputArray pano)
{
    Status status = estimateTransform(images);
    if (status != OK)
        return status;
    return composePanorama(pano);
}


OpenCVStitcher::Status OpenCVStitcher::stitch(cv::InputArrayOfArrays images, const std::vector<std::vector<cv::Rect> > &rois, cv::OutputArray pano)
{
    Status status = estimateTransform(images);
    if (status != OK)
        return status;
    return composePanorama(pano);
}



OpenCVStitcher::Status OpenCVStitcher::estimateCameraParams()
{
    /* TODO OpenCV ABI 4.x
    get rid of this dynamic_cast hack and use estimator_
    */
    startProfiling("homography estimation");
    cv::Ptr<cv::detail::Estimator> estimator;
    if (dynamic_cast<cv::detail::AffineBestOf2NearestMatcher*>(m_featuresMatcher.get()))
        estimator = cv::makePtr<cv::detail::AffineBasedEstimator>();
    else
        estimator = cv::makePtr<cv::detail::HomographyBasedEstimator>();

    if (!(*estimator)(m_features, pairwise_matches_, cameras_))
        return ERR_HOMOGRAPHY_EST_FAIL;
    endProfiling("homography estimation");

    for (size_t i = 0; i < cameras_.size(); ++i)
    {
        cv::Mat R;
        cameras_[i].R.convertTo(R, CV_32F);
        cameras_[i].R = R;
    }
    for (size_t i = 0; i < pairwise_matches_.size(); i++) {
        if ((pairwise_matches_[i].src_img_idx != (pairwise_matches_[i].dst_img_idx + 1)) && (pairwise_matches_[i].dst_img_idx != (pairwise_matches_[i].src_img_idx + 1))) {
            pairwise_matches_[i].confidence = 0;
        }
    }

    startProfiling("bundle adjustment");
    bundle_adjuster_->setConfThresh(conf_thresh_);
    if (!(*bundle_adjuster_)(m_features, pairwise_matches_, cameras_))
        return ERR_CAMERA_PARAMS_ADJUST_FAIL;
    endProfiling("bundle adjustment");

    // Find median focal length and use it as final image scale
    startProfiling("find focal length");
    std::vector<double> focals;
    for (size_t i = 0; i < cameras_.size(); ++i)
    {
        focals.push_back(cameras_[i].focal);
    }

    std::sort(focals.begin(), focals.end());
    if (focals.size() % 2 == 1)
        warped_image_scale_ = static_cast<float>(focals[focals.size() / 2]);
    else
        warped_image_scale_ = static_cast<float>(focals[focals.size() / 2 - 1] + focals[focals.size() / 2]) * 0.5f;

    if (do_wave_correct_)
    {
        std::vector<cv::Mat> rmats;
        for (size_t i = 0; i < cameras_.size(); ++i)
            rmats.push_back(cameras_[i].R.clone());
        cv::detail::waveCorrect(rmats, wave_correct_kind_);
        for (size_t i = 0; i < cameras_.size(); ++i)
            cameras_[i].R = rmats[i];
    }
    endProfiling("find focal length");

    return OK;
}


